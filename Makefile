CC=g++

all: srftp clftp

srftp: srftp.o
	$(CC) -g -o srftp srftp.o

clftp: clftp.o
	$(CC) -g -o clftp clftp.o

srftp.o: srftp.cpp
	$(CC) -g -Wall -c srftp.cpp 

clftp.o: clftp.cpp
	$(CC) -g -Wall -c clftp.cpp 

tar: 
	tar cvf project6.tar clftp.cpp srftp.cpp Makefile README performance.jpg 

clean:
	rm -f srftp clftp *.o


