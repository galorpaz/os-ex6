/*
    C socket server example
*/
 
#include <stdio.h>
#include <errno.h>
#include <string.h>    
#include <sys/socket.h>
#include <arpa/inet.h> 
#include <unistd.h>    
#include <stdlib.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/select.h>
#include <map>
#include <time.h>
#include <sys/time.h>

using namespace std; 

static int socket_desc = -1;
static int client_sock = -1;

int exit_flag = 0;


/**
 * read a data from the soket
 */
static int read_sock(int sock, char *buf, int length, bool any_size=false) {
	int read =0, n;
	bool read_somthing = false;
	//printf("sock=%d, length=%d, any_size=%d\n", sock, length, any_size);	
	while(read < length) {
		n =  recv(sock , buf+read , length - read , 0);
		//n =  recv(sock , buf+read , 1024 , 0);
		//printf("recv = n");		
		if (n > 0 )  {
			read +=n;
			read_somthing = true;
		}
		else if (n < 0) {
			fprintf(stderr, "ERROR: while reading from sockect\n");
			break;
		}
		else {
			if (read_somthing && any_size)
				break;
		}
		
	}
	return read;
}

class client_descriptor {
	public:
	 	char * file_name;
		int file_size;	
	private:	
		int fd;
		int cur_pos;
		char* client_data;
		int sock;
public:
	client_descriptor(int cl_sock): file_name(NULL),  file_size(0), fd(-1), cur_pos(0), client_data(NULL), sock(cl_sock)
    {;}
	int read_header();
	int read_data();
	int write_data();
	void cl_close();
	~client_descriptor() {
		cl_close();
		if (file_name != NULL)
			free (file_name);
		if (client_data != NULL)
			free(client_data);
	}
};

/**
 *  read the file size, the name size and the file name from the client
 */
int  client_descriptor::read_header()
{
	int n, file_name_size;
    //read the file size in to file_size
	if ((n=read_sock(sock, (char*)&file_size, sizeof(int))) != sizeof(int)) {
		fprintf(stderr, "ERROR: while reading file size from sockect\n");
		return -1;
	}
	
	//read the file name size in to file_name_size 
	if ((n=read_sock(sock, (char*)&file_name_size, sizeof(int))) != sizeof(int)) {
		fprintf(stderr, "ERROR: while reading file name size from sockect\n");
		return -1;
	}
	if ((file_name_size == 0) & (file_size==0)) 
	{
		fprintf(stderr, "ERROR: file_size=0 and file_name_size=0 \n");
		return -1;
	}
	//increment the file_name, since according to the sending protocol it add /0 char
	// before it start sending the file content
	file_name_size++;
	file_name = (char*)malloc(file_name_size);
	if (file_name == NULL) { //allocation failed
		fprintf(stderr, "ERROR: allocation memory\n");
		return -1;
	}
	//read the file name in to name
	if ((n=read_sock(sock, file_name, file_name_size)) != file_name_size) {
		fprintf(stderr, "ERROR: while reading file name  from sockect\n");
		return -1;
	}
	fd = open(file_name, O_CREAT | O_TRUNC | O_WRONLY, 0770);
	if (fd < 0) {
		fprintf(stderr,"ERROR: while open file_name=%s\n",file_name);
		return(-1);
	}
	client_data = (char*)malloc(file_size);
	if (client_data == NULL) {
		fprintf(stderr,"ERROR: while allocat client_data\n");
		return(-1);
	}	
	return 0;
}

/*
 *	returns:  -1: error
 *			   0: do somthing
 *			   99: read completed
 */ 
int client_descriptor::read_data()
{
	int n;
	int ret =0;	
	printf("client_descriptor::read_data() before read_sock\n");
			
	while (cur_pos<file_size) {
		if ((n=read_sock(sock, client_data + cur_pos, 1024, true)) < 0) {
			fprintf(stderr,"ERROR: while allocat client_data\n");
			return(-1);
		}
		cur_pos += n;
		if (cur_pos == file_size) {
			ret = 99;
			break;
		}			
	}	
	
/*
	if ((n=read_sock(sock, client_data + cur_pos, file_size-cur_pos, true)) < 0) {
		fprintf(stderr,"ERROR: while allocat client_data\n");
		return(-1);
	}
	cur_pos += n;
	if (cur_pos == file_size) {
		ret = 99;
	}
*/
	//printf("ret is = %d", ret);
	return ret;
}

int client_descriptor::write_data()
{
	int res = write(fd, client_data, file_size);
	if (res != file_size) {
		fprintf(stderr,"ERROR: while write to file_name=%s\n",file_name);
		return(-1);
	}
	printf("file=%s was written succesfully\n\n", file_name);
	return 0;
}
/*
 * close the client fd and client socket
 */
void client_descriptor::cl_close()
{
	if (fd >= 0) {	
		close(fd);
		fd = -1;
	}
	if (client_sock  >= 0) {
		close(client_sock);
		client_sock = -1;
	}
	printf("finish close for file = %s\n", file_name);
}
/*
 * set exit_flag to -1 when ctrl c signal was caught
 */
void ctrl_c_handler(int s){
	printf("Caught signal %d\n",s);    
	exit_flag = -1;
}

static map<int, client_descriptor*> sock2client;
/**
 * iterate over the map and clean the sock2client map
 */
void clean_up() {
	printf("enter clean_up\n");	
	map<int, client_descriptor*>:: iterator it;
	for (it = sock2client.begin(); it != sock2client.end(); ++it) {
		if (it->first > 0) {
			delete (it->second);
			sock2client.erase(it);		
		}
	}	
}

int main(int argc , char *argv[])
{
    int  c , res,i, fd_max;
	bool do_accept=false;
    struct sockaddr_in server , client;
	struct timeval start, end;	

	int port_num = 8888;
	fd_set client_fd_set;
	fd_set copy;
	client_descriptor *p_client;

	if (argc > 1) {
		port_num = atoi(argv[1]);
	} else {
		fprintf(stderr, "ERROR: use default port number 8888\n");
	}
	
	if (port_num < 0) {
		fprintf(stderr, "ERROR: server-port is illeagl\n");
		exit (-1);
	}
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = ctrl_c_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);

	//Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");
     
    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( port_num );
     
    //Bind
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        perror("bind failed. Error");
        return 1;
    }
    puts("bind done");
    FD_ZERO(&client_fd_set); 
	FD_ZERO(&copy);
    //Listen
    listen(socket_desc , 100);
	// add the socke decri so the select will include new client connection
    FD_SET(socket_desc, &client_fd_set);
	fd_max = socket_desc;
    //Accept and incoming connection
    puts("Waiting for incoming connections...");
    c = sizeof(struct sockaddr_in);
    while (exit_flag == 0) { 
	    	
		//accept connection from an incoming client
		if (do_accept) {
			gettimeofday(&start, NULL);			
			client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
			if (client_sock < 0)
			{
		 	   perror("accept failed");
		 	   exit(1);
			}
			puts("Connection accepted\n");
		 	
			
			//Receive a message from client
			p_client = new client_descriptor(client_sock);
			res = p_client->read_header();
			if (res != 0 ) {
				fprintf(stderr,"ERROR: while reading from client header\n");
				delete p_client;
				continue;
			}
			
			printf("Going to get data for file %s size %d\n", p_client->file_name, p_client->file_size);
		
			sock2client[client_sock] = p_client;
			FD_SET(client_sock, &client_fd_set);
			if (client_sock > fd_max) {
				fd_max = client_sock;
			}
		}
		
		do_accept = false;
		//FD_SET(socket_desc, &client_fd_set);
		copy = client_fd_set;		
		printf("before select\n");
		res = select (fd_max+1, &copy, NULL, NULL,  NULL);
		printf("after select res=%d errno=%d\n", res, errno);

		if (res < 0) {
			if (errno == 4)
				break;
		  fprintf (stderr, "ERROR: on select\n");
		  exit (1);
		}
		
		if (res == 0) {
			usleep(100);
			continue;
		}
		//iterating over to array to check for the readt filed to read
		printf ("fd_max=%d\n", fd_max);
		for (i = 0; i < fd_max+1; ++i) {
             if (FD_ISSET (i, &copy))
             {
				printf("handle=%d\n", i);					                 
				if (i == socket_desc) {
					printf("request for connect\n");					                 
					do_accept = true;
					continue;
				 }	
				p_client = sock2client[i];
				if (p_client != NULL){
					printf("before read \n");
					if (p_client->read_data() == 99) {
						printf("after read \n");							
						p_client->write_data();
						p_client->cl_close(); 
						sock2client.erase(i);	//remove the client from the list
						printf("erase from map\n");				
						FD_CLR(i, &client_fd_set); // clear the fd from client_fd_set
						printf("clear from set\n");
						delete (p_client);
						gettimeofday(&end, NULL);
						printf("%ld\n", ((end.tv_sec * 1000000 + end.tv_usec)
		  				- (start.tv_sec * 1000000 + start.tv_usec)));   					
					}
				}
			}
		}
		
	}
	close(socket_desc);
	socket_desc = -1;
	clean_up(); 
	
	return 0;
}
